const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/ 


//Solution 1
const movieName = Object.entries(favouritesMovies)
                .filter((movie)=>
              {
                  let earning = movie[1].totalEarnings.substring(1,movie[1].totalEarnings.length -1);
                  return earning>500;
              });

// const Solution1=movieWithEarningOver500M(favouritesMovies);
//console.log(movieName);


// Solution 2
//console.log('--------------------------------------------');

 const movieNameWithOscar = movieName.filter((movie)=>
    {
        let earning = movie[1].totalEarnings.substring(1,movie[1].totalEarnings.length -1);
        let oscarNom= movie[1].oscarNominations;
        return earning>500 && oscarNom>3;
    });

// const Solution2=movieWithOscarNominiesOver3(favouritesMovies);
//console.log(movieNameWithOscar);


// Solution 3
//console.log('--------------------------------------------');


    const movieWithLeoD = Object.entries(favouritesMovies)
                          .filter((movie)=>
                        {
                            return movie[1].actors.includes("Leonardo Dicaprio");
                        });

// const Solution3=movieWithLeoD(favouritesMovies);
//console.log(movieWithLeoD);



// Solution 4
//console.log('--------------------------------------------');

    const sortMovieWithHigherRating=Object.entries(favouritesMovies).sort((ratingA,ratingB)=>
    {
        let movieRatingA=ratingA[1].imdbRating;
        let movieRatingB=ratingB[1].imdbRating;

        if(movieRatingA<movieRatingB)
        {
            return 1;
        }else if(movieRatingA>movieRatingB){
            return -1
        }else{
            
            let earningA = ratingA[1].totalEarnings.substring(1,ratingA[1].totalEarnings.length -1);
            let earningB = ratingB[1].totalEarnings.substring(1,ratingB[1].totalEarnings.length -1);

            return earningB-earningA;
        }
    });


// const Solution4=sortMovieWithHigherRating(favouritesMovies);
//console.log(sortMovieWithHigherRating);

//console.log("-----------------------------------------------")
//Solution 5

function checkEntries(array) {
  return Object.fromEntries(array)
}

const groupMoviesBasedOnGenre = Object.keys(favouritesMovies)
    .map(movie => {

        let genres = favouritesMovies[movie].genre

        if (genres.includes("drama")) {
            return ['drama', movie, favouritesMovies[movie]]
        }
        else if (genres.includes("sci-fi")) {
            return ['sci-fi', movie, favouritesMovies[movie]]
        }
        else if (genres.includes("adventure")) {
            return ['adventure', movie, favouritesMovies[movie]]
        }
        else if (genres.includes("thriller")) {
            return ['thriller', movie, favouritesMovies[movie]]
        }
        else {
            return ['crime', movie, favouritesMovies[movie]]

        }
    })
    .reduce((output, movieDetails) => {

        output[movieDetails[0]].push(checkEntries([[movieDetails[1], movieDetails[2]]]))
        return output
    }, {
        drama: [],
        'sci-fi': [],
        adventure: [],
        thriller: [],
        crime: []
    })
 //console.log(groupMoviesBasedOnGenre);


// console.log(solution5);
// console.log("--------------------------------------")
// console.log("--------------------------------------")
// console.log("--------------------------------------")
// console.log("--------------------------------------")

